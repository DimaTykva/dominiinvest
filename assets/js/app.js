let openingsListShow = [
    {"name":"User Acquisition Specialist" , "location": "Istanbul, Turkey", "workPlace": "Remote", "workType": "Full Time", "department": "Marketing"},
    {"name":"Designer" , "location": "Istanbul, Turkey", "workPlace": "Office", "workType": "Full Time", "department": "Marketing"},
    {"name":"Web Developer" , "location": "Istanbul, Turkey", "workPlace": "Office", "workType": "Full Time", "department": "IT"},
    {"name":"Regional Ambassador" , "location": "Frankfurt, Germany", "workPlace": "Office", "workType": "Full Time", "department": "Marketing"},
    {"name":"Team Lead Sales Manager" , "location": "Istanbul, Turkey", "workPlace": "Office", "workType": "Full Time", "department": "Sales"},
    {"name":"Driver" , "location": "Istanbul, Turkey", "workPlace": "Remote", "workType": "Part Time", "department": "Logistic"},
    {"name":"Video Editor" , "location": "Istanbul, Turkey", "workPlace": "Remote", "workType": "Part Time", "department": "Marketing"},
    {"name":"Finance Analitic" , "location": "Istanbul, Turkey", "workPlace": "Office", "workType": "Full Time", "department": "Finance"},
    {"name":"Media Buyer" , "location": "Istanbul, Turkey", "workPlace": "Office", "workType": "Full Time", "department": "Marketing"},
    {"name":"HR manager" , "location": "Istanbul, Turkey", "workPlace": "Office", "workType": "Full Time", "department": "HR"}
]

class Post {
    constructor(imgSrc, title, text, href, date) {
        this.imgSrc = imgSrc;
        this.title = title;
        this.text = text;
        this.href = href;
        this.date = date;
    }
}


const App = {
    data() {
        return{
            productItems: [
                {imgSrc:'assets/img/products/urea.svg', title: 'Urea', text: 'Urea, also known as carbamide, is an organic compound with chemical formula CO(NH₂)₂'},
                {imgSrc:'assets/img/products/iron_ore.svg', title: 'Iron ore', text: 'Rocks and minerals from which metallic iron can be economically extracted'},
                {imgSrc:'assets/img/products/alumminium.svg', title: 'Aluminum', text: 'Aluminum (Al), also spelled aluminium, chemical element, a lightweight silvery white metal of main Group 13 (IIIa, or boron group) of the periodic table'},
                {imgSrc:'assets/img/products/ammonia.svg', title: 'Ammonia', text: 'Compound of nitrogen and hydrogen with the formula NH3'},
                {imgSrc:'assets/img/products/nickel_ore.svg', title: 'Nickel ore', text: 'Silvery-white lustrous metal with a slight golden tinge'}
            ],
            benefitList: [
                {imgSrc:'assets/img/benefits/low_prices.svg', title:'Low prices', text:'We provide the lowest prices for goods and services in the field of transportation of raw materials.'},
                {imgSrc:'assets/img/benefits/fast_delivery.svg', title:'Fast delivery', text:'It is because of the speed of delivery that we are the leader. Delivery time is very important to us and our customers.'},
                {imgSrc:'assets/img/benefits/individual_terms.svg', title:'Individual terms', text:'For each client, individual conditions of cooperation are provided.'},
                {imgSrc:'assets/img/benefits/preparation_of_documents.svg', title:'Preparation of documents', text:'Our company employs highly qualified lawyers who draw up contracts for our clients every day.'},
                {imgSrc:'assets/img/benefits/from_point_to_point.svg', title:'Delivey from point to point', text:'Each client can receive delivery to any point within the company\'s area of work.'},
                {imgSrc:'assets/img/benefits/safe_transaction.svg', title:'Safe transactions', text:'All money transfers in accordance with the agreement are carried out only by trusted services and banks.'}
            ],
            productPageItems: [
                {imgSrc:'../assets/img/products/urea.svg', title: 'Urea', text: 'Urea, also known as carbamide, is an organic compound with chemical formula CO(NH₂)₂', href: 'urea'},
                {imgSrc:'../assets/img/products/ammonia.svg', title: 'Ammonia', text: 'Compound of nitrogen and hydrogen with the formula NH3', href: 'ammonia'},
                {imgSrc:'../assets/img/products/iron_ore.svg', title: 'Iron ore', text: 'Rocks and minerals from which metallic iron can be economically extracted', href: 'iron_ore'},
                {imgSrc:'../assets/img/products/alumminium.svg', title: 'Aluminum', text: 'Aluminum (Al), also spelled aluminium, chemical element, a lightweight silvery white metal of main Group 13 (IIIa, or boron group) of the periodic table', href: 'aluminium'},
                {imgSrc:'../assets/img/products/nickel_ore.svg', title: 'Nickel ore', text: 'Silvery-white lustrous metal with a slight golden tinge', href: 'nickel_ore'},
                {imgSrc:'../assets/img/products/sulfate.svg', title: 'Sulfate', text: 'Sulfate is an inert molecule that must be chemically activated before it can be involved in biochemical pathways.', href: 'sulfate'},
                {imgSrc:'../assets/img/products/potassium_nitrate.svg', title: 'Potassium nitrate', text: 'Is a crystalline salt, KNO3; a strong oxidizer used especially in making gunpowder, as a fertilizer, and in medicine.', href: 'potasium_nitrate'},
                {imgSrc:'../assets/img/products/petroleum.svg', title: 'Petroleum', text: 'Petroleum, also known as crude oil and oil, is a naturally occurring, yellowish-black liquid found in geological formations.', href: 'petroleum'}
            ],
            structureList: [
                {imgSrc:'../assets/img/aboutus/metalurgy.png', title: 'Metallurgy', link:'#metalurge', text: 'The Group transports coke products, pig iron, steel semi-finished products, long and flat products, urea and other raw materials to any part of the world. All products are of high quality and are certified.'},
                {imgSrc:'../assets/img/aboutus/sales.png', title: 'Sales', link:'#Sales', text: 'Our sales and service network spans over 100 countries and provides over 15,000 customers with high quality products and services.'},
                {imgSrc:'../assets/img/aboutus/logistic.png', title: 'Logistic and procurement', link:'#Logistic', text: 'Domini Invest Group is developing a supply management system: it provides production with raw materials, materials and equipment, and also supplies steel products to consumers. Own transport and forwarding company and shipping lines allow us to promptly supply raw materials and products around the world.'},
                {imgSrc:'../assets/img/aboutus/manage.png', title: 'management', link:'#management', text: 'Our top management team brings together manufacturing and sales professionals with broad expertise in finance and business management.'}
            ],
            departmentList: ['Any department', 'Marketing', 'IT', 'Sales', 'Logistic', 'Finance', 'HR'],
            locationList: ['Any location', 'Istanbul, Turkey', 'Frankfurt, Germany'],
            openingsListShow: openingsListShow,
            cityClassNameList: [
                {cityClassName1: 'show-map-pin madrid', cityClassName2: 'map__invest-item madrid', location: 'Madrid, Spain'},
                {cityClassName1: 'show-map-pin badajoz', cityClassName2: 'map__invest-item badajoz', location: 'Badajoz, Spain'},
                {cityClassName1: 'show-map-pin lyon', cityClassName2: 'map__invest-item lyon', location: 'Lyon, Spain'},
                {cityClassName1: 'show-map-pin bratislava', cityClassName2: 'map__invest-item bratislava', location: 'Bratislava, Slovakia'},
                {cityClassName1: 'show-map-pin rzeszow', cityClassName2: 'map__invest-item rzeszow', location: 'Rzeszow, Poland'},
                {cityClassName1: 'show-map-pin dusseldorf', cityClassName2: 'map__invest-item dusseldorf', location: 'Dusseldorf, Germany'},
                {cityClassName1: 'show-map-pin luxembourg', cityClassName2: 'map__invest-item luxembourg', location: 'Luxembourg'},
                {cityClassName1: 'show-map-pin ghent', cityClassName2: 'map__invest-item ghent', location: 'Ghent, Belgium'},
                {cityClassName1: 'show-map-pin zurich', cityClassName2: 'map__invest-item zurich', location: 'Zurich, Switzerland'},
                {cityClassName1: 'show-map-pin riga', cityClassName2: 'map__invest-item riga', location: 'Riga, Latvia'},
                {cityClassName1: 'show-map-pin st-peterburg', cityClassName2: 'map__invest-item st-peterburg', location: 'St Peterburg, Russia'},
                {cityClassName1: 'show-map-pin eskisehir', cityClassName2: 'map__invest-item eskisehir', location: 'Eskisehir, Turkey'},
                {cityClassName1: 'show-map-pin astrahen', cityClassName2: 'map__invest-item astrahen', location: 'Astrahan, Russia'},
                {cityClassName1: 'show-map-pin kirov', cityClassName2: 'map__invest-item kirov', location: 'Kirov, Russia'},
                {cityClassName1: 'show-map-pin ufa', cityClassName2: 'map__invest-item ufa', location: 'Ufa, Russia'},
                {cityClassName1: 'show-map-pin perm', cityClassName2: 'map__invest-item perm', location: 'Perm, Russia'},
                {cityClassName1: 'show-map-pin krasnoyarsk', cityClassName2: 'map__invest-item krasnoyarsk', location: 'Krasnoyarsk, Russia'},
                {cityClassName1: 'show-map-pin ulaanbaatar', cityClassName2: 'map__invest-item ulaanbaatar', location: 'Ulaanbaatar, Mongolia'},
                {cityClassName1: 'show-map-pin yakutsk', cityClassName2: 'map__invest-item yakutsk', location: 'Yakutsk, Russia'},
                {cityClassName1: 'show-map-pin caracas', cityClassName2: 'map__invest-item caracas', location: 'Caracas, Venezuela'},
                {cityClassName1: 'show-map-pin maracay', cityClassName2: 'map__invest-item maracay', location: 'Maracay, Venezuela'},
                {cityClassName1: 'show-map-pin valencia', cityClassName2: 'map__invest-item valencia', location: 'Valencia, Venezuela'},
                {cityClassName1: 'show-map-pin oklahoma', cityClassName2: 'map__invest-item oklahoma', location: 'Oklahoma, USA'},
                {cityClassName1: 'show-map-pin edmonton', cityClassName2: 'map__invest-item edmonton', location: 'Edmonton, Canada'},
                {cityClassName1: 'show-map-pin b-columbia', cityClassName2: 'map__invest-item b-columbia', location: 'British Columbia, Canada'},
                {cityClassName1: 'show-map-pin yukon', cityClassName2: 'map__invest-item yukon', location: 'Yukon, Canada'}
            ],
            search: '',
            postList: [
                new Post ('../assets/img/blog-page/rectangle_14_560.png',
                 'Non-ferrous metallurgy of Western Europe',
                 'Brief description of non-ferrous metallurgy. Non-ferrous metals - in technology, metals and alloys that are not ferrous (that is, everything except iron and its alloys). Non-ferrous metallurgy is a branch of metallurgy that includes the extraction, enrichment of non-ferrous metal ores and the smelting of non-ferrous metals and their alloys. By physical properties and purpose, non-ferrous metals can be conditionally divided into heavy (copper, lead, zinc, tin, nickel) and light (aluminum, titanium, magnesium). Based on this division, the metallurgy...',
                 '../articles/Non-ferrous_metallurgy-of_Western_Europe',
                 'May 21, 2022'),
                new Post ('../assets/img/blog-page/rectangle_14_copy_563.png',
                 'Prices for carbamide in the world are falling',
                 'There is a downward trend on the world urea market in January - this fertilizer is rapidly becoming cheaper in all regions. Suppliers need to sell the available volumes, and they reduce the selling prices. In addition, many countries continue to experience a seasonal lull and purchases are inactive. Last year, carbamide quotes reached multi-year highs, and now they are correcting down...',
                 '../articles/Prices_for_carbamide_in_the_world_are_falling',
                 'May 19, 2022'),
                new Post ('../assets/img/blog-page/rectangle_14_copy_2_567.png',
                 'Week 4: Coking coal prices hit historic highs',
                 'Analysis of the impact of news for January 24-28, 2022 on the global mining and metals complex from the GMK. Center Last week was marked by rising prices for raw materials and finished products. Coking coal prices in Australia have reached an all-time high. American companies expect to maintain a positive conjuncture in 2022. Global steel production outside of China has recovered to 2019 levels, but it is difficult to expect further growth...',
                 '../articles/Week-4_Coking_coal-prices_hit_historic_highs',
                 'May 16, 2022'),
                new Post ('../assets/img/blog-page/rectangle_14_copy_3_589.png',
                 'Australia expects ore exports to grow by 6.1% in 2022-2023',
                 'The increase in supply is due to start production at new mines in Western Australia Ministry of Industry, Innovation and Science Australia (OCE) expects growth of export of iron ore by 6.1% in the years 2022-2023 as compared to 2020-2021 years - to 920 million tons...',
                 '../articles/Blog_article_Australia_expects_ore_exports_to_grow_by_6_1_in_2022',
                 'May 15, 2022'),
                new Post ('../assets/img/blog-page/rectangle_14_copy_3_593.png',
                 'Turkish company Kardemir launched a new steel plant',
                 'The enterprise has an annual capacity of 1.2 million tons of billets Turkish steelmaker Kardemir Çelik Sanayi A.Ş. announced the start of production at its new steel plant, in which investments began in 2020. It is reported by SteelOrbis...',
                 '../articles/Turkish_company_Kardemir_launched_a_new_steel_plant',
                 'May 12, 2022')
            ],
            articlesMainPage: [
                new Post ('assets/img/blog-page/rectangle_14_560.png',
                    'Non-ferrous metallurgy of Western Europe',
                    'Brief description of non-ferrous metallurgy. Non-ferrous metals - in technology, metals and alloys that are not ferrous (that is, everything except iron and its alloys). Non-ferrous metallurgy is a branch of metallurgy that includes the extraction, enrichment of non-ferrous metal ores and the smelting of non-ferrous metals and their alloys. By physical properties and purpose, non-ferrous metals can be conditionally divided into heavy (copper, lead, zinc, tin, nickel) and light (aluminum, titanium, magnesium). Based on this division, the metallurgy...',
                    'articles/Non-ferrous_metallurgy-of_Western_Europe',
                    'May 21, 2022'),
                new Post ('assets/img/blog-page/rectangle_14_copy_563.png',
                    'Prices for carbamide in the world are falling',
                    'There is a downward trend on the world urea market in January - this fertilizer is rapidly becoming cheaper in all regions. Suppliers need to sell the available volumes, and they reduce the selling prices. In addition, many countries continue to experience a seasonal lull and purchases are inactive. Last year, carbamide quotes reached multi-year highs, and now they are correcting down...',
                    'articles/Prices_for_carbamide_in_the_world_are_falling',
                    'May 19, 2022'),
                new Post ('assets/img/blog-page/rectangle_14_copy_2_567.png',
                    'Week 4: Coking coal prices hit historic highs',
                    'Analysis of the impact of news for January 24-28, 2022 on the global mining and metals complex from the GMK. Center Last week was marked by rising prices for raw materials and finished products. Coking coal prices in Australia have reached an all-time high. American companies expect to maintain a positive conjuncture in 2022. Global steel production outside of China has recovered to 2019 levels, but it is difficult to expect further growth...',
                    'articles/Week-4_Coking_coal-prices_hit_historic_highs',
                    'May 16, 2022')
            ]
        }
    },
    methods : {
        scrollToForm(blockID){
            document.querySelector(blockID).scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            })
        },
        showMobileNavbar(){
            document.querySelector('.header__navbar__list-mobile').classList.toggle('mobile_show');
            document.querySelector('.burger_menu-open').classList.toggle('close');
        },
        showAboutText(){
            document.querySelector('.about__main__top-info-more-text').classList.toggle('more-text-show');
        },
        openSelList(){
            document.querySelector('#departments .sel-list').classList.toggle('active');
        },
        filterCareer3: function(evt){
            // var val = evt.target.value;
            var valDep = document.getElementById('departments').value;
            var valLoc = document.getElementById('location').value;
            if (valDep == 'Any department' && valLoc == 'Any location') {
                this.openingsListShow = openingsListShow;

            }else if(valDep == 'Any department' && valLoc !== 'Any location'){
                this.openingsListShow = openingsListShow.filter(function (e){ return e.location == valLoc;});
                console.log(this.openingsListShow)
            }else if(valDep !== 'Any department' && valLoc == 'Any location'){
                this.openingsListShow = openingsListShow.filter(function (e){ return e.department == valDep;});
                console.log(this.openingsListShow)
            }else if(valDep !== 'Any department' && valLoc !== 'Any location'){
                this.openingsListShow = openingsListShow.filter(function (e){ return e.department == valDep;}).filter(function (e) {
                    return e.location == valLoc;
                });
                console.log(this.openingsListShow)
            }
        },
        submitForm(e){
            let formId = e.target.id;
            var xhr = new XMLHttpRequest();
            var url = 'http://localhost:3000';
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    var json = JSON.parse(xhr.responseText);
                    console.log(json);
                }
            };
               let firstname =  document.querySelector("#" + formId+ '  ' + "input[name=firstname]").value,
                lastname =  document.querySelector("#" + formId+ '  ' + "input[name=lastname]").value,
                email = document.querySelector("#" + formId+ '  ' + "input[name=email]").value,
                phone =  document.querySelector("#" + formId+ '  ' + "input[name=phone]").value,
                country = document.querySelector("#" + formId+ '  ' + "input[name=country]").value,
                subject =document.querySelector("#" + formId+ '  ' + "input[name=subject]").value,
                message_text = document.querySelector("#" + formId+ '  ' + "input[name=message_text]").value
            var data = {
                "firstname":  firstname,
                "lastname":  lastname,
                "email": email,
                "phone":  phone,
                "country":  country,
                "subject":  subject,
                "message_text":  message_text
            }
            console.log('hello' + data.firstname)
            var data = JSON.stringify(data);
            xhr.send(data);
            e.target.reset();
            document.getElementById('preloader').style.display = 'flex';
        },
        closeSuccess(){
            document.getElementById('preloader').style.display = 'none';
        }

    },
    computed: {
        filteredList() {
            return this.postList.filter(post => {
                return post.title.toLowerCase().includes(this.search.toLowerCase())
            })
        }
    }
}

const mobileHeader = {
    data() {
        return {
            methods : {
                showMobileNavbar()
                {
                    document.querySelector('.header__navbar__list-mobile').classList.toggle('mobile_show');
                    document.querySelector('.burger_menu-open').classList.toggle('close');
                }
            }
        }
    }
}


Vue.createApp(App).mount('#app');
Vue.createApp(mobileHeader).mount('#mobile-header');