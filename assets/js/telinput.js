let searchCountry = ['tw'],
    errorMsg1 = document.querySelector(".error-msg"),
    validMsg = document.querySelector("#valid-msg");
var btn = document.querySelector("input[type=submit], button[type=submit]");
var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
var countryCode;
window.addEventListener("load", function () {
    // UPDATE: use a public static field so we can fudge it in the tests
    window.intlTelInputGlobals.windowLoaded = true;
});
var input = document.querySelectorAll("input[name='phone2']");

input.forEach(function(e){
    var iti = intlTelInput(e, {
        utilsScript: "../../build/js/utils.js?1613236686837",
        autoHideDialCode: false,
        separateDialCode: true,
        preferredCountries:[""],
        autoPlaceholder: "aggressive",
        placeholderNumberType: "MOBILE",
        initialCountry: "auto",
        // onlyCountries: searchCountry,
        customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
            return selectedCountryPlaceholder
                .replace(/ /g, '')
                .replace(/-/g, '').replace(/\(/g, '')
                .replace(/\)/g, '');
        },
        geoIpLookup: function (success, failure) {
            $.get("https://ipinfo.io", function () {
            }, "jsonp").always(function (resp) {
                countryCode = (resp && resp.country) ? resp.country : "CA";

                success(countryCode);
            });
        }
    });

    $(e).on('input', function(e){
        $(this).closest('form').find("input[name='phone']").val(iti.getNumber(intlTelInputUtils.numberFormat.E164));
    });
    $(e).on('countrychange', function(e){
        $.get("https://ipinfo.io", function () {
        }, "jsonp").always(function (resp) {
            countryCode = (resp && resp.country) ? resp.country : iti.getSelectedCountryData().iso2;
            $("input[name='country']").val(countryCode.toUpperCase());
        });
    });

    var reset = function() {
        $(input).removeClass("error");
        $(errorMsg1).innerHTML = "";
        $(errorMsg1).addClass("hide");
        $(validMsg).addClass("hide");
    };

    // on blur: validate
    $(e).on('focus', function() {
        reset();
        $(btn).removeAttr('disabled');
    });

    $(e).on('keyup', function() {
        // reset();
        if ($.trim($(e).val())) {
            if (iti.isValidNumber()) {
                $(e).removeClass("error");
            } else {
                $(e).addClass("error");
                var errorCode = iti.getValidationError();
                $(errorMsg1).innerHTML = errorMap[errorCode];
                $(errorMsg1).removeClass("hide");
            }
        }
    });

});